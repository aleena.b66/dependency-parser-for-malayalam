സിനിമ	N_NN_S_NU	NP
അവസാനിച്ച്	V_VM_VF	VGNF
ഇരുന്ന	V_VM_VNF	VGNF
സമയം	N_NN	NP
ആളുകൾ	N_NN_S_NU	NP
ബഹളം	N_NN_O_NU	NP
വച്ചു	V_VM	VGF
.	RD_PUNC	O
സുഹൃത്തുക്കള്‍ക്കൊപ്പം	N_NN	NP
സഹകരിക്കുന്നുവെന്ന്	V_VM_VNF	VGNF
മാത്രം	RP_RPD	NP
ഞാൻ	PR_PRP	NP
അതിൽ	DM_DMD	NP
സഹായിച്ചെന്ന്	V_VM_VNF	VGNF
മാത്രം	RP_RPD	NP
പിന്നീട്	CC_CCD	NP
രണ്ട്	QT_QTC	O
കഥകള്‍	N_NN	NP
കൂടി	PSP	CCP
ഉള്‍ക്കൊള്ളിച്ചു	V_VM_VF	VGF
തിരുവനന്തപുരത്തുണ്ടെന്ന്	V_VM_VNF	VGNF
ഞാനറിഞ്ഞതേയില്ല	V_VM_VF	VGF
ജ്യോതിഷിയും	N_NN	NP
ആയിരുന്ന	V_VM_VNF	VGNF
പ്രമുഖ	JJ	JJP
വ്യക്തി	N_NN_S_NU	NP
ഇന്നലെ	N_NN	NP
അന്തരിച്ചു	V_VM_VF	VGF
അതോ	CC_CCD	NP
എന്റെ	PR_PRP	NP
തോന്നലോ	N_NN	NP
?	RD_PUNC	O
വേറിട്ട	JJ	JJP
കാഴ്ചകൾ	N_NN	NP
കണ്ട്	V_VM_VNF	VGNF
കണ്ണുകൾ	N_NN	NP
ഈറൻ	V_VM_VNF	VGNF
അണിയുന്നു	V_VM_VF	VGF
ഉജ്ജയിനിയിലെ	N_NNP	NP
ശിവശങ്കരനെ	N_NNP_S_M	NP
നമസ്കരിച്ചാൽ	V_VM_VNF	VGNF
ജീവിതത്തി	N_NN	NP
മരണചിന്ത	N_NN_O_NU	NP
ഉണ്ടാകില്ല	RP_NEG	NP
എന്ന്	CC_CCD	NP
പറയപെടുന്നു	V_VM_VF	VGF
കുറച്ചുപേരെങ്കിലും	N_NN	NP
ഈ	DM_DMD	NP
ഉത്തരവാദിത്വം	N_NN	NP
ഏറ്റെടുക്കുവാൻ	V_VM_VNF	VGNF
മുന്നോട്ടുവരും	V_VM_VF	VGF
ഇടുക്കി	N_NNP	NP
വന്യജീവിസങ്കേതം	N_NN_S_NU	NP
തൊടുപുഴയിൽ	N_NNP	NP
ഉടുന്പന്‍ചോല	N_NNP	NP
താലൂക്കിൽ	N_NN_O_NU	NP
കാണപ്പെടുന്നു	V_VM_VF	VGF
കളിച്ചുനടക്കേണ്ട	V_VM_VNF	VGNF
പ്രായത്തിൽ	N_NN	NP
രവിശങ്കര്‍	N_NNP	NP
എന്ന	CC_CCS	NP
കുട്ടിയുടെ	N_NN	NP
ഈ	DM_DMD	NP
വാക്കുകള്‍	N_NN	NP
ആരും	N_NN	NP
ചെവിക്കൊണ്ടില്ല	V_VM_VF	VGF
തിലക	N_NNP_S_M	NP
നാടകം	N_NN	NP
ജീവിതഗന്ധി	N_NNP_O_M	NP
ആയിരുന്നു	V_VM_VF	VGF
നിരാശ	N_NN	NP
ബാധിച്ച്	V_VM_VNF	VGNF
പുസ്തകവായന	N_NN	NP
പോലും	PSP	CCP
ഉപേക്ഷിച്ചു	V_VM_VF	VGF
കുറിപ്പുകളെഴുതുകയാണെന്ന	V_VM_VNF	VGNF
നാട്യത്തി	N_NN	NP
ഞാന്‍	PR_PRP	NP
കഥയെഴുതി.	V_VM_VF	VGF
ജീവിതത്തി	N_NN	NP
ഏറ്റവും	RP_INTF	NP
ഗതികെട്ട്	N_NN	NP
നില്‍ക്കുന്ന	V_VM_VNF	VGNF
സന്ദര്‍ഭത്തിലാണ്	V_VAUX	VGF
ആദ്യ	QT_QTF	O
കഥയെഴുതുന്നത്	V_VM_VNF	VGNF
ഇന്നലെ	N_NST	NP
എന്നെ	PR_PRP	NP
വിളിച്ചത്	V_VM_VNF	VGNF
ഓര്‍ക്കാപ്പുറത്തായിരുന്നു	V_VM_VF	VGF
രാജാവിന്റെ	N_NNP	NP
ജീവിതം	N_NN_S_NU	NP
എല്ലാവരും	QT_QTF	O
മാതൃക	N_NN_O_NU	NP
ആക്കിയിരുന്നു	V_VM_VF	VGF
വീണ്ടും	RB	NP
എഴുത്ത്	N_NN	NP
തുടങ്ങിയപ്പോ	V_VM_VNF	VGNF
സ്വന്തം	PR_PRF	NP
നാട്ടിലേക്കുള്ള	N_NN	NP
തിരിഞ്ഞുനോട്ടമാണ്	V_VAUX	VGF
എന്നെ	PR_PRP	NP
സഹായിച്ചത്	V_VM_VNF	VGNF
ആദ്യം	N_NN	NP
നിര്യാതനായി	V_VM_VNF	VGNF
എന്ന	CC_CCS	NP
കഥ	N_NN	NP
മാത്രം	RP_RPD	NP
ചെയ്യാനായിരുന്നു	V_VM_VF	VGF
സഞ്ജുവിന്റെ	N_NN	NP
പദ്ധതി	N_NN	NP
ലോകം	N_NN	NP
മുഴുവന്‍	QT_QTF	O
എനിക്ക്	PR_PRP	NP
കുടുംബങ്ങളുണ്ട്	V_VAUX	VGF
അതുകൊണ്ട്	CC_CCD	NP
കുറേക്കാലം	N_NN	NP
എഴുത്ത്	N_NN	NP
നിര്‍ത്തി	V_VM_VF	VGF
വേറൊരു	JJ	JJP
രംഗത്തും	N_NN	NP
രക്ഷപ്പെടില്ലെന്നുള്ളത്	V_VM_VNF	VGNF
കൊണ്ട്	PSP	CCP
എഴുത്തുകാരനായ	JJ	JJP
ആളാണ്	N_NN	NP
ഞാന്‍	PR_PRP	NP
ഒരു	QT_QTC	O
തുരുത്തി	N_NN	NP
അകപ്പെട്ടാ	V_VM_VNF	VGNF
പിന്നെ	CC_CCD	NP
വള്ളം	N_NN	NP
ഉണ്ടായാലേ	V_VM_VNF	VGNF
രക്ഷയുള്ളൂ	V_VM_VF	VGF
അത്	DM_DMD	NP
മാതൃഭൂമി	N_NN	NP
വിഷുപ്പതിപ്പി	N_NN	NP
പ്രസിദ്ധീകരിച്ചു	V_VM_VF	VGF
2	QT_QTC	O
ഇടുക്കി	N_NNP	NP
വന്യജീവിസങ്കേതം	N_NN_S_NU	NP
തൊടുപുഴയില്‍	N_NNP	NP
ഉടുന്പന്‍ചോല	N_NNP	NP
താലൂക്കില്‍	N_NN_O_NU	NP
കാണപ്പെടുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
3	QT_QTC	O
ജ്യോതിഷിയും	N_NN	NP
ആയിരുന്ന	V_VM_VNF	VGNF
പ്രമുഖ	JJ	JJP
വ്യക്തി	N_NN_S_NU	NP
ഇന്നലെ	N_NN	NP
അന്തരിച്ചു	V_VM_VF	VGF
.	RD_PUNC	O
4	QT_QTC	O
തിലകന്റെ	N_NNP_S_M	NP
നാടകം	N_NN	NP
ജീവിതഗന്ധി	N_NNP_O_M	NP
ആയിരുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
5	QT_QTC	O
രാവിലെ	N_NN	NP
വിദ്യാലയത്തില്‍	N_NN_S_NU	NP
കൃത്യം	N_NN	NP
മണി	N_NN_O_NU	NP
അടിക്കുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
6	QT_QTC	O
ഉജ്ജയിനിയിലെ	N_NNP	NP
ശിവശങ്കരനെ	N_NNP_S_M	NP
നമസ്കരിച്ചാല്‍	V_VM_VNF	VGNF
ജീവിതത്തില്‍	N_NN	NP
മരണചിന്ത	N_NN_O_NU	NP
ഉണ്ടാകില്ല	RP_NEG	NP
എന്ന്	CC_CCD	NP
പറയപെടുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
7	QT_QTC	O
സിനിമ	N_NN_S_NU	NP
അവസാനിച്ച്	V_VM_VF	VGF
ഇരുന്ന	V_VM_VNF	VGNF
സമയം	N_NN	NP
ആളുകള്‍	N_NN_S_NU	NP
ബഹളം	N_NN_O_NU	NP
വച്ചു	V_VM_VNF	VGNF
.	RD_PUNC	O
8	QT_QTC	O
ആശാന്‍	N_NNP	NP
സ്ക്വയറിൽ	N_NNP	NP
ആണ്	V_VAUX	VGF
അവന്‍	PR_PRP	NP
താമസിച്ച്	V_VM_VNF	VGNF
ഇരുന്ന	V_VM_VNF	VGNF
വീട്‌	N_NN_O_NU	NP
കണ്ടത്	V_VM_VNF	VGNF
.	RD_PUNC	O
9	QT_QTC	O
രാജാവിന്റെ	N_NNP	NP
ജീവിതം	N_NN_S_NU	NP
എല്ലാവരും	QT_QTF	O
മാതൃക	N_NN_O_NU	NP
ആക്കിയിരുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
10	QT_QTC	O
വേറിട്ട	JJ	JJP
കാഴ്ചകള്‍	N_NN	NP
കണ്ട്	V_VM_VNF	VGNF
കണ്ണുകള്‍	N_NN	NP
ഈറൻ	V_VM_VNF	VGNF
അണിയുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
11	QT_QTC	O
ക്രിസ്തീയ	N_NNP	NP
വിശ്വാസ	N_NN	NP
പ്രകാരം	PSP	NP
ജറുസലീമിൽ	N_NN_O_NU	NP
അണ്	V_VAUX	VGF
യേശു	N_NN	NP
പിറന്നത്	V_VM_VNF	VGNF
.	RD_PUNC	O
12	QT_QTC	O
കവിത	N_NN	NP
ഉദിക്കുന്ന	V_VM_VNF	VGNF
മനസ്സില്‍	N_NN	NP
സന്തോഷം	N_NN_O_NU	NP
മാത്രമേ	RP_RPD	NP
ഉണ്ടാവൂ	V_VM_VF	VGF
.	RD_PUNC	O
13	QT_QTC	O
മഴ	N_NN	NP
കാരണം	PSP	NP
കേരളത്തിലെ	N_NNP	NP
നദികളുടെ	N_NN	NP
ജലനിരപ്പ്	N_NN	NP
ഉയരുന്നു	V_VM_VF	VGF
2168	QT_QTC	O
ബാലന്‍	N_NN_S_M	NP
മറഞ്ഞ്	V_VM_VNF	VGNF
പോയതിന്	V_VM_VNF	VGNF
ശേഷം	RP_RPD	NP
ഭഗവാന്‍റെ	N_NN	NP
അനുഭൂതി	N_NN_O_NU	NP
ആളുകള്‍ക്ക്	N_NN	NP
അവിടെ	DM_DMD	NP
ഉണ്ടായി	V_VM_VF	VGF
2169	QT_QTC	O
കാലാന്തരത്തില്‍	N_NN_S_NU	NP
ഈ	DM_DMD	NP
ക്ഷേത്രം	N_NN_O_NU	NP
ഉണ്ടാക്കി	V_VM_VF	VGF
2170	QT_QTC	O
1683ല്‍	QT_QTC	O
ഈസ്റ്റിന്ത്യാ	N_NN	NP
കമ്പനി	N_NN_S_NU	NP
തലശ്ശേരിയുടെ	N_NNP_O_NU	NP
കൂടെ	PSP	CCP
നിരന്തരമായ	JJ	JJP
സമ്പര്‍ക്കം	N_NN	NP
പുലര്‍ത്തി	V_VM_VNF	VGNF
വന്നിരുന്നു	V_VM_VF	VGF
2171	QT_QTC	O
1703ല്‍	QT_QTC	O
കമ്പനി	N_NN_S_NU	NP
തലശ്ശേരിയില്‍	N_NNP	NP
ഒരു	QT_QTF	NP
കോട്ട	N_NN_O_NU	NP
ഉണ്ടാക്കി	V_VM_VF	VGF
2172	QT_QTC	O
അതിനു	DM_DMD	NP
ശേഷം	PSP	NP
തലശ്ശേരിയിലെ	N_NNP_S_NU	NP
ഈ	DM_DMD	NP
കോട്ടയുടെ	N_NN	NP
ചുറ്റുപാടുമുള്ള	N_NN	NP
പട്ടണങ്ങള്‍	N_NN_O_NU	NP
വികസിച്ചു	V_VM_VF	VGF
2173	QT_QTC	O
ഈ	DM_DMD	NP
കോട്ട	N_NN_S_NU	NP
ചരിത്രത്തിന്‍റെ	N_NNP	NP
അധ്യായങ്ങളില്‍	N_NN_O_NU	NP
പ്രിയപ്പെട്ടതാണ്	V_VAUX	VGF
2174	QT_QTC	O
തലശ്ശേരിയിലെ	N_NNP_S_NU	NP
ഈ	DM_DMD	NP
കോട്ടയുടെ	N_NN	NP
ചുമരുകള്‍	N_NN_O_NU	NP
വളരെ	RB	NP
ഉയരമുള്ളതാണ്	V_VAUX	VGF
2175	QT_QTC	O
തലശ്ശേരിയിലെ	N_NNP	NP
ഈ	DM_DMD	NP
കോട്ടയില്‍	N_NN_S_NU	NP
അനേകം	QT_QTF	O
രഹസ്യ	N_NN	NP
തുരങ്കങ്ങള്‍	N_NN_O_NU	NP
ഉണ്ട്	V_VAUX	VGF
സമുദ്രത്തിലേക്ക്	N_NN_O_NU	NP
തുറന്നിരിക്കുന്നു	V_VM_VF	VGF
2176	QT_QTC	O
വളപട്ടണം	N_NNP_S_NU	NP
തടിവ്യാപാരത്തില്‍	N_NN_O_NU	NP
മുഴുവനായ	QT_QTF	NP
ഏഷ്യയിലെ	N_NNP	NP
പ്രധാനപ്പെട്ട	RB	NP
പഴയ	RB	NP
വ്യാപാര	N_NN	NP
സ്ഥലമാണ്.	V_VAUX	VGF
2177	QT_QTC	O
വളപട്ടണം	N_NNP	NP
നദിയുടെ	N_NN	NP
തീരത്തുള്ള	N_NN	NP
ഘോരവനങ്ങളില്‍	N_NN_S_NU	NP
നിന്നും	PSP	NP
കിട്ടിയ	V_VM_VNF	VGNF
തേക്ക്	N_NNP_O_NU	NP
ഈട്ടി	N_NNP_O_NU	NP
ഇഴുവാ	N_NNP_O_NU	NP
മുരിക്ക്	N_NNP_O_NU	NP
ചന്ദനം	N_NNP_O_NU	NP
മുതലായ	CC_CCS	NP
വൃക്ഷങ്ങളുടെ	N_NN	NP
തടികളാണ്	V_VAUX	VGF
2178	QT_QTC	O
വളപട്ടണത്തിലെ	N_NNP	NP
തടികള്‍	N_NN	NP
വെസ്റ്റേണ്‍	N_NN	NP
ഇന്ത്യ	N_NN	NP
ഫ്ലൈവുഡ്	N_NN	NP
ലിമിറ്റഡ്	N_NN_S_NU	NP
വളപട്ടണത്തിലെ	N_NNP	NP
തടി	N_NN	NP
വ്യാപാര	N_NN	NP
കന്പനി	N_NN	NP
തെക്ക്	N_NNP	NP
-	RD_PUNC	NP
കിഴക്കന്‍	N_NNP	NP
ഏഷ്യയിലെ	N_NNP	NP
പ്രശസ്തമായ	JJ	JJP
തടി	N_NN	NP
വ്യാപാര	N_NN	NP
കന്പനി	N_NN_O_NU	NP
ആണ്	V_VAUX	VGF
2179	QT_QTC	O
വളപട്ടണത്തില്‍	N_NNP_S_NU	NP
ചായ	N_NN_O_NU	NP
കാപ്പി	N_NN_O_NU	NP
പുകയില	N_NN_O_NU	NP
കശുവണ്ടി	N_NN_O_NU	NP
മുതലായവയുടെ	CC_CCS	NP
കൃഷി	N_NN	NP
ഉണ്ട്	V_VAUX	VGF
2180	QT_QTC	O
വളപട്ടണത്തിന്‍റെ	N_NNP	NP
പേരും	N_NN	NP
വരുന്നുണ്ട്	V_VAUX	VGF
2181	QT_QTC	O
രണ്ടാമതായി	QT_QTO	O
ആകര്‍ഷിക്കുന്നത്	V_VM_VNF	VGNF
മത്സ്യബന്ധന	N_NN	NP
കേന്ദ്രം	N_NN	NP
ജലവിതരണ	N_NN	NP
കേന്ദ്രം	N_NN	NP
1	QT_QTC	O
ഉന്മേഷമുള്ള	JJ	JJP
ശ്വാസവും	N_NN	NP
,	RD_PUNC	O
തിളങ്ങുന്ന	JJ	JJP
പല്ലുകളും	N_NN	NP
താങ്കളുടെ	PR_PRP	NP
വ്യക്തിത്വത്തെ	N_NN	NP
ശോഭിപ്പിക്കുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
2	QT_QTC	O
പല്ലുകളിൽ‍	N_NN	NP
നിന്ന്	PSP	NP
താങ്കളുടെ	PR_PRP	NP
ആത്മവിശ്വാസവും	N_NN	NP
വർ‍ദ്ധിക്കുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	NP
3	QT_QTC	NP
നമ്മുടെ	PR_PRP	NP
മോണയുടേയും	N_NN	NP
,	RD_PUNC	NP
പല്ലിന്‍റേയും	N_NN	NP
ഇടയിൽ‍	N_NST	NP
ബാക്ടീരിയ	N_NN	NP
ഉണ്ടാകുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
4	QT_QTC	O
ഇവ	DM_DMD	NP
പല്ലുകളെ	N_NN	NP
ചീത്തയാക്കുകയും	V_VM_VNF	VGNF
ശ്വാസത്തിൽ‍	N_NN	NP
ദുർ‍ഗന്ധം	N_NN	NP
ഉണ്ടാക്കുകയും	V_VM_VNF	VGNF
ചെയ്യുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
5	QT_QTC	O
ഇവിടെ	DM_DMD	NP
കൊടുത്തിട്ടുള്ള	V_VM_VNF	VGNF
കുറച്ച്	QT_QTF	O
എളുപ്പമുള്ള	JJ	JJP
മരുന്ന്	N_NN	NP
കുറിപ്പടികളുടെ	N_NN	NP
സഹായത്തോടെ	V_VM_VNF	VGNF
താങ്കൾ‍ക്ക്	PR_PRP	NP
താങ്കളുടെ	PR_PRP	NP
പല്ലുകളെ	N_NN	NP
ശുചിയായും	N_NN	NP
,	RD_PUNC	NP
ശ്വാസത്തെ	N_NN	NP
ഉന്മേഷമുള്ളതായും	N_NN	NP
ആക്കാന്‍	V_VM_VNF	VGNF
കഴിയും	V_VM_VF	VGF
.	RD_PUNC	O
6	QT_QTC	O
പൽലുകളെ	N_NN	NP
നല്ല	JJ	JJP
രീതിയിൽ‍	N_NN	NP
വൃത്തിയാക്കു	V_VM_VNF	VGNF
.	RD_PUNC	O
7	QT_QTC	O
പല്ലുകളെ	N_NN	NP
നല്ല	JJ	JJP
രീതിയിൽ‍	N_NN	NP
വൃത്തിയാക്കുന്നതിന്	V_VM_VNF	VGNF
രണ്ട്	QT_QTC	O
മുതൽ‍	N_NST	NP
മൂന്ന്	QT_QTC	O
മിനുട്ട്	N_NN	NP
വരെ	N_NST	NP
സമയം	N_NN	NP
വേണം	N_NN	NP
.	RD_PUNC	O
8	QT_QTC	O
പക്ഷേ	CC_CCD	NP
മിക്ക	QT_QTF	O
ആളുകളും	N_NN	NP
ഇതിനുവേണ്ടി	CC_CCD	NP
ഒരു	QT_QTF	O
മിനുട്ടിലും	N_NN	NP
കുറഞ്ഞ	JJ	JJP
സമയമാണ്	N_NN	NP
എടുക്കുന്നത്	V_VM_VNF	VGNF
.	RD_PUNC	O
9	QT_QTC	O
ധാരാളം	QT_QTF	O
വെള്ളം	N_NN	NP
കുടിക്കൂ	V_VM_VF	VGF
.	RD_PUNC	O
10	QT_QTC	O
വായ	N_NN	NP
ഉണങ്ങുമ്പോൾ	V_VM_VNF	VGNF
ബാക്ടീരിയായുടെ	N_NN	NP
ആക്രമണം	V_VM_VNF	VGNF
വേഗത്തിൽ‍	RB	NP
നടക്കുന്നു	V_VM_VINF	VGNF
.	RD_PUNC	O
11	QT_QTC	O
ഇതിനാൽ‍	CC_CCD	NP
ശ്വാസത്തിൽ‍	N_NN	NP
ദുർ‍ഗന്ധം	N_NN	NP
ഉണ്ടാകുന്നതാണ്	V_VM_VNF	VGNF
.	RD_PUNC	O
12	QT_QTC	O
ധാരാളം	QT_QTF	O
വെള്ളം	N_NN	NP
കുടിക്കുന്നതുകൊണ്ട്	V_VM_VNF	VGNF
ആഹാരത്തിന്റെ	N_NN	NP
അവശിഷ്ടങ്ങൾ‍	N_NN	NP
വൃത്തിയാക്കുക	V_VM_VNF	VGNF
മാത്രമല്ല	RP_NEG	NP
ഉമിനീരും	N_NN	NP
ഉണ്ടാക്കുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
13	QT_QTC	O
വായ്	N_NN	NP
വൃത്തിയായി	V_VM_VNF	VGNF
സൂക്ഷിക്കുന്നതിന്	V_VM_VNF	VGNF
ഉമിനീരിന്	N_NN	NP
പ്രധാന	N_NN	NP
പങ്ക്	N_NN	NP
ഉണ്ട്	V_VAUX	VGF
.	RD_PUNC	O
14	QT_QTC	O
ഉമിനീർ	N_NN	NP
ആ	DM_DMD	NP
ബാക്ടീരിയാകളെ	N_NN	NP
നശിപ്പിക്കുന്നു	V_VM_VNF	VGNF
അവ	DM_DMD	NP
ശ്വാസത്തിൽ	N_NN	NP
ദുർഗന്ധം	N_NN	NP
ഉണ്ടാക്കുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
15	QT_QTC	O
ചവയ്ക്കു	V_VM_VF	VGF
മധുരമില്ലത്ത	JJ	JJP
ചൂയിങ്ഗമ്മ്	N_NN	NP
.	RD_PUNC	O
16	QT_QTC	O
ചൂയിങ്ഗമ്മ്	N_NN	NP
ചവയ്ച്ചാൽ	V_VM_VNF	VGNF
ഉമിനീർ	N_NN	NP
ഉണ്ടാകുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
17	QT_QTC	O
ചൂയിങ്ഗമ്മ്	N_NN	NP
പല്ലുകളെ	N_NN	NP
വൃത്തിയായി	RB	NP
സൂക്ഷിക്കുന്നതിന്	V_VM_VNF	VGNF
സഹായിക്കുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
18	QT_QTC	O
മധുരമുള്ള	JJ	JJP
ഗമ്മ്	N_NN	NP
ആരോഗ്യത്തിന്	N_NN	NP
നല്ലതല്ലെന്ന്	RP_NEG	NP
പറയപ്പെടുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
19	QT_QTC	O
അതുകൊണ്ട്	CC_CCD	O
ഡെന്റിസ്റ്റ്	N_NN	NP
മധുരമുള്ള	JJ	JJP
ഗമ്മ്	N_NN	NP
കഴിക്കുന്നതിനുള്ള	V_VM_VNF	VGNF
ഉപദേശം	N_NN	NP
നൽകാറില്ല	V_VM_VNF	VGNF
.	RD_PUNC	O
20	QT_QTC	O
പതിവായിട്ട്	N_NN	NP
ദന്തനിരീക്ഷണം	N_NN	NP
ചെയ്യണം	V_VM_VF	VGF
.	RD_PUNC	O
21	QT_QTC	O
ദന്തപരിശോധന	N_NN	NP
ദന്തഡോക്ടറെ	N_NNP	NP
കൊണ്ട്	PSP	NP
പതിവായി	RB	NP
നടത്തണം	V_VM_VINF	VGNF
.	RD_PUNC	O
22	QT_QTC	O
അവ	DM_DMD	NP
പല്ലുകളുടെ	N_NN	NP
ചെറിയ-ചെറിയ	RD_ECH	NP
പ്രശ്നങ്ങളെ	N_NN	NP
എളുപ്പത്തിൽ‍	N_NN	NP
പരിഹരിക്കാന്‍	V_VM_VNF	VGNF_INF
കഴിയും	V_VM_VF	VGF
.	RD_PUNC	O
23	QT_QTC	O
കഴിച്ചതിനുശേഷം	N_NN	NP
വായ്	N_NN	NP
വൃത്തിയാക്കണം	V_VM_VF	VGF
.	RD_PUNC	O
24	QT_QTC	O
എല്ലായ്പ്പോഴും	N_NN	NP
കഴിച്ചതിനുശേഷം	N_NN	NP
വെള്ളം	N_NN	NP
കൊണ്ട്	RP_RPD	NP
വായ്	N_NN	NP
തീർ‍ച്ചയായും	RP_INTF	NP
കഴുകണം	V_VM_VNF	VGNF
.	RD_PUNC	O
25	QT_QTC	O
ഇതിനാൽ‍	CC_CCD	NP
ആഹാരത്തിന്റെ	N_NN	NP
അവശിഷ്ടങ്ങൾ‍	N_NN	NP
വൃത്തിയാക്കുന്നു	V_VM_VF	VGF
.	RD_PUNC	O
26	QT_QTC	O
ചെറുനാരങ്ങയുടേയും	N_NN	NP
,	RD_PUNC	O
ഉപ്പിന്റേയും	N_NN	NP
ചേരുവ	N_NN	NP
കൊണ്ട്	RP_RPD	NP
പല്ലുകളെ	N_NN	NP
വൃത്തിയാക്കു	V_VM_VF	VGF
.	RD_PUNC	O
27	QT_QTC	O
ഒരു	QT_QTF	O
ചെറിയ	JJ	JJP
സ്പൂണ്‍	N_NN	NP
ഉപ്പ്	N_NN	NP
എടുത്തിട്ട്	V_VM_VNF	VGNF
അതിൽ‍	DM_DMD	NP
മൂന്നോ	QT_QTC	O
നാലോ	QT_QTC	O
തുള്ളി	N_NN	NP
നാരങ്ങാനീർ	N_NN	NP
ഒഴിക്കുക	V_VM_VINF	VGNF
.	RD_PUNC	O
28	QT_QTC	O
എല്ലാ	QT_QTF	O
ആഴ്ചയിലും	N_NN	NP
ഈ	DM_DMD	NP
മിശ്രിതം	N_NN	NP
കൊണ്ട്	RP_RPD	NP
പല്ലുകളെ	N_NN	NP
വൃത്തിയാക്കു	V_VM_VF	VGF
.	RD_PUNC	O
29	QT_QTC	O
ഇതുകൊണ്ട്	CC_CCD	NP
മാത്രം	RP_RPD	NP
പല്ലുകൾ‍	N_NN	NP
തിളങ്ങുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O
30	QT_QTC	O
മാത്രല്ല	RP_NEG	NP
ശ്വാസത്തിലെ	N_NN	NP
ദുർ‍ഗന്ധത്തിനും	N_NN	NP
മോചനം	N_NN	NP
ലഭിക്കുന്നു	V_VM_VNF	VGNF
.	RD_PUNC	O

